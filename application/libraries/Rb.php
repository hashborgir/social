<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rb {

	protected $CI;

	function __construct() {
		// Include database configuration
		$this->CI = & get_instance();

		require_once APPPATH . 'third_party/rb.php';
		// Database data
		$this->CI->load->database();
		if (!R::testConnection()) {
			R::setup("mysql:host=" . $this->CI->db->hostname . ";dbname=" . $this->CI->db->database, $this->CI->db->username, $this->CI->db->password);
			if (ENVIRONMENT === 'development') {
				R::debug(TRUE, 3);
			}
		}
	}

}
