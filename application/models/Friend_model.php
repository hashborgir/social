<?php

class Friend_model extends MY_Model {

	public $id;
	public $userid;
	public $friendid;
	public $added;

	public function __construct() {
		parent::__construct();
	}

	public function addfriend($id) {
		$sql = "SELECT * FROM friend WHERE userid=? AND friendid=?";
		$query = $this->db->query($sql, [$_SESSION['user']['userid'], $id]);
		$row = $query->row();

		if (empty($row)) {
			$data = [
				'userid' => $_SESSION['user']['userid'],
				'friendid' => $id,
				'added' => date('Y-m-d H:i:s')
			];
			$this->db->insert('friend', $data);
			$return['user'] = $this->get_friends($_SESSION['user']['userid']);
			$return['current_user'] = $this->get_friends($id);
			return $return;
		}
		return false;
	}

	public function listnewmembers() {
		$now = date('Y-m-d H:i:s');
		$lastmonth = date('Y-m-d H:i:s', strtotime('-10 years'));
		$sql = 'SELECT
					profile.id AS id,
					user.id AS userid,
					firstname,
					lastname,
					username,
					joindate,
					email,
					image
				FROM
					profile
						LEFT JOIN
					user ON profile.userid = user.id
				WHERE
						joindate >= ? AND joindate <= ?
					AND
						user.id != ?
					LIMIT 10';

		return $this->db->query($sql, [$lastmonth, $now, $_SESSION['user']['userid']])->result_array();
	}

	public function is_friend($friendid, $userid) {
		$sql = "SELECT
					*
				FROM
					friend
				WHERE
					friendid = ? AND userid = ?";
		$query = $this->db->query($sql, array($friendid, $userid));

		if (count($query->result_array()) > 0) {
			return true;
		}
		return false;
	}

}
