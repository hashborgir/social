<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

	public $id;
	public $email;
	public $password;
	public $admin;

	public function __construct() {
		parent::__construct();
	}

	public function add($email, $password, $firstname, $lastname) {
		$hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);

		$sql = "SELECT * FROM user WHERE email=?";
		$query = $this->db->query($sql, [$email]);
		$user = $query->row();

		// if user does not exist
		if (!$user) {
			$user = new User_model();
			$user->email = $email;
			$user->password = $hash;
			$user->admin = 0;
		}

		$res = $this->db->insert('user', $user);

		// if user is inserted into 'user' table
		// add user's profile data
		if ($res) {
			$this->profile->add($this->db->insert_id(), $firstname, $lastname);
			return $this->db->insert_id();
		}

		return false;
	}

	public function verify_login($email, $password) {

		$sql = "SELECT * FROM user WHERE email=?";
		$query = $this->db->query($sql, [$email]);
		$user = $query->row();

		if ($user) {
			$hash = $user->password;
		}

		$verified = password_verify($password, $hash);

		if ($verified) {
			return $user->id;
		}
		return false;
	}

}
