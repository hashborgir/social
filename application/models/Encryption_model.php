<?php

class Encryption_model extends MY_Model {

	public $path = null;
	public $public_key = null;

	public function __construct($id = null) {
		parent::__construct($id);

		$this->path = APPPATH . "../keys/$id/";
	}

	public function genkeys() {
		$private_key = openssl_pkey_new([
			'private_key_bits' => 4096,
			'private_key_type' => OPENSSL_KEYTYPE_RSA
		]);
		openssl_pkey_export_to_file($private_key, $this->path . 'private.key');
		$public_key = openssl_pkey_get_details($private_key);
		file_put_contents(getcwd() . '/public.key', $public_key['key']);
		openssl_free_key($private_key);
	}

	public function getkey() {
		$this->public_key = openssl_get_publickey(file_get_contents($this->path . 'public.key'));
	}

	public function encrypt($string) {
		openssl_seal($string, $encrypted_text, $encrypted_keys, [$key]);
		return $encrypted_text;
	}

	public function decrypt($string) {

	}

}
