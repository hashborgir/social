<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends MY_Model {

	public $id;
	public $from;
	public $to;
	public $date;
	public $message;
	public $viewed;

	public function __construct() {
		parent::__construct();
	}

	public function send($id, $message) {
		$data['from'] = $_SESSION['user']['userid'];
		$data['to'] = $id;
		$data['date'] = date('Y-m-d H:i:s');
		$data['viewed'] = 0;
		$data['message'] = urldecode($message);

		$this->db->insert('message', $data);
	}

	public function get_messages($id) {
		$sql = 'SELECT
					message.id,
					date,
					firstname,
					lastname,
					username,
					image,
					message,
					viewed
				FROM
					message
						LEFT JOIN
					profile ON message.from = profile.userid
				WHERE
					(`from` = ? AND `to` = ?)
						OR (`from` = ? AND `to` = ?) ORDER BY date ASC';
		return $this->db->query($sql, array($_SESSION['user']['userid'], $id, $id, $_SESSION['user']['userid']))->result_array();
	}

	public function mark_as_read($id) {
		$sql = "UPDATE message SET viewed=1 WHERE id=?";
		$this->db->query($sql, [$id]);
	}

	public function get_unread($id) {
		$sql = "SELECT
					COUNT(*) as unread
				FROM
					message
				WHERE
					`to` = ? AND viewed = 0";
		return $this->db->query($sql, [$id])->row()->unread;
	}

	public function get_inbox() {
		$sql = "SELECT DISTINCT
					`from`,
					message.id AS messageid,
					`date`,
					message,
					viewed,
					userid,
					firstname,
					lastname,
					username,
					image
				FROM
					message
						LEFT JOIN
					profile ON message.`from` = profile.userid
				WHERE
					`to` = ?
				GROUP BY `from`";
		$inbox['received'] = $this->db->query($sql, [$_SESSION['user']['userid']])->result_array();
		$sql = "SELECT DISTINCT
					`from`,
					message.id AS messageid,
					`date`,
					message,
					viewed,
					userid,
					firstname,
					lastname,
					username,
					image
				FROM
					message
						LEFT JOIN
					profile ON message.`to` = profile.userid
				WHERE
					`from` = ?
				GROUP BY `to`";
		$inbox['sent'] = $this->db->query($sql, [$_SESSION['user']['userid']])->result_array();
		return $inbox;
	}

}
