<?php

class Admin_model extends MY_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_all_users($offset, $limit) {
		$sql = "SELECT
					user.id,
					username,
					email,
					admin,
					firstname,
					lastname,
					joindate,
					location,
					image
				FROM
					user
						LEFT JOIN
					profile ON user.id = profile.userid
				LIMIT ?, ?";
		return R::getAll($sql, array($offset, $limit));
	}

	public function count_all_users() {
		return R::getCell("SELECT count(*) FROM user");
	}

}
