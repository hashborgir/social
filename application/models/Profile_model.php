<?php

class Profile_model extends MY_Model {

	public $userid;
	public $firstname;
	public $lastname;
	public $username;
	public $joindate;
	public $location;
	public $image;
	public $about;
	public $website;
	public $skype;
	public $profession;
	public $relationship;
	public $dob;
	public $phone;

	public function get_profile($username = null, $id = null) {
		$sql = "SELECT
					user.id as id,
					user.id AS userid,
					email,
					firstname,
					lastname,
					username,
					joindate,
					location,
					image,
					about,
					website,
					skype,
					profession,
					relationship,
					location,
					dob,
					phone,
					admin
				FROM
					profile
						LEFT JOIN
					user ON profile.userid = user.id
				WHERE
					username = ? OR userid=?";
		return $this->db->query($sql, [$username, $id])->row_array();
	}

	public function search($string) {

		$sql = "SELECT
					user.id as id,
					user.id AS userid,
					email,
					firstname,
					lastname,
					username,
					joindate,
					location,
					image,
					about,
					website,
					skype,
					profession,
					relationship,
					location,
					dob,
					phone
				FROM
					profile
						LEFT JOIN
					user ON profile.userid = user.id
				WHERE
					username LIKE 'tom' OR email LIKE ?
						OR firstname LIKE ?
						OR lastname LIKE ?";
		return $this->db->query($sql, [$string, $string, $string])->result_array();
	}

	public function get_profile_by_id($id) {
		$sql = "SELECT
					user.id as id,
					user.id AS userid,
					email,
					firstname,
					lastname,
					username,
					joindate,
					location,
					image,
					about,
					website,
					skype,
					profession,
					relationship,
					location,
					dob,
					phone,
					admin
				FROM
					profile
						LEFT JOIN
					user ON profile.userid = user.id
				WHERE
					userid = ?";
		return $this->db->query($sql, [$id])->row_array();
	}

	public function update($id, $data) {
		$data = $this->input->post();
		unset($data['submit']);

		$config['upload_path'] = FCPATH . 'img/' . $_SESSION['user']['userid'] . '/';
		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|webp';
		$config['max_size'] = '1000';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$config['file_name'] = "profile.jpg";
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);

		if ($_FILES['image']['size'] > 0) {
			if (!$this->upload->do_upload('image')) {
				echo $this->upload->display_errors();
			} else {
				$upload_data = $this->upload->data();
			}
			$data['image'] = "/img/$id/" . $config['file_name'];
		}

		$sql = "SELECT * FROM profile WHERE userid=?";
		$profile = $this->db->query($sql, [$id])->row();

		if (!$profile) {
			$profile = new Profile_model();
		}

		foreach ($data as $k => $v) {
			if ($v == '') {
				unset($k);
			} else {
				if ($k == 'username') {
					$v = str_replace(' ', '', $v);
				}
				$profile->$k = $v;
				$_SESSION['user'][$k] = $v;
			}
		}

		$this->db->where('userid', $id);
		$res = $this->db->update('profile', $profile);

		if ($res) {
			return true;
		}
		return false;
	}

	public function username_exists($username) {
		if (!$username) return false;
		$sql = "SELECT username FROM profile WHERE username=?";
		$username = $this->db->query($sql, [$username])->result_array();

		if (count($username) > 0) {
			return true;
		}
		return false;
	}

	public function add($id, $firstname, $lastname) {
		$profile = new Profile_model();

		$profile->userid = $id;
		$profile->firstname = $firstname;
		$profile->lastname = $lastname;
		$username = "$firstname$lastname";
		if ($this->username_exists($username)) {
			$profile->username = "$firstname$lastname" . rand(10000, 99999);
		} else {
			$profile->username = "$firstname$lastname";
		}
		$profile->joindate = date('Y-m-d H:i:s');
		$profile->location = "The Universe!";
		$profile->about = null;
		$profile->website = null;
		$profile->skype = null;
		$profile->profession = null;
		$profile->relationship = null;
		$profile->location = null;
		$profile->dob = null;
		$profile->phone = null;

		$res = $this->db->insert('profile', $profile);

		if ($res) {
			return true;
		}
		return false;
	}

	public function get_unread_message_count($id) {
		$sql = "SELECT
					count(*) as unread
				FROM
					message
				WHERE
					viewed = 0 AND `to` = ?";
		return $this->db->query($sql, [$id])->row()->unread;
	}

	public function get_followers($id) {
		$sql = "SELECT
				user.id AS userid,
				email,
				firstname,
				lastname,
				username,
				joindate,
				about,
				website,
				skype,
				profession,
				relationship,
				location,
				dob,
				phone,
				image
			FROM
				friend
					LEFT JOIN
				user ON user.id = friend.userid
					LEFT JOIN
				profile ON user.id = profile.userid
			WHERE
				friend.friendid = ?";
		return $this->db->query($sql, [$id])->result_array();
	}

	public function get_following($id) {
		$sql = "SELECT
				user.id AS userid,
				email,
				firstname,
				lastname,
				username,
				joindate,
				about,
				website,
				skype,
				profession,
				relationship,
				location,
				dob,
				phone,
				image
			FROM
				friend
					LEFT JOIN
				user ON user.id = friend.friendid
					LEFT JOIN
				profile ON user.id = profile.userid
			WHERE
				friend.userid = ?";
		return $this->db->query($sql, [$id])->result_array();
	}

}
