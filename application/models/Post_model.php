<?php

class Post_model extends MY_Model {

	public $id;
	public $from;
	public $to;
	public $date;
	public $likes;
	public $post;
	public $date_updated;

	public function __construct() {
		parent::__construct();
	}

	public function create($id, $post_content) {
		$post = new Post_model();

		$post->from = $_SESSION['user']['userid'];
		$post->to = $id;

		$post->date = date('Y-m-d H:i:s');
		$post->likes = 0;

		$post->post = $post_content;
		$post->date_updated = date('Y-m-d H:i:s');

		$res = $this->db->insert('post', $post);

		if ($res) {
			return ['id' => $id, 'content' => $post_content];
		}
		return false;
	}

	public function read($id) {
		$sql = '
			SELECT
				post.id,
				userid,
				username,
				firstname,
				lastname,
				post,
				date,
				likes,
				image
			FROM
				post
					LEFT JOIN
				profile ON post.from = profile.userid
			WHERE
				`to` = ?
			ORDER BY `date` DESC';
		return $this->db->query($sql, [$id])->result_array();
	}

	public function update($id, $content) {
		$sql = "SELECT * FROM post WHERE id=?";
		$query = $this->db->query($sql, [$id]);

		$post = $query->row();
		$post->post = $content;
		$post->date_updated = date('Y-m-d H:i:s');

		$this->db->where('id', $id);
		$res = $this->db->update('post', $post);
		if ($res) {
			return true;
		}
		return false;
	}

	public function delete($id) {
		$this->db->where('id', $id);
		$res = $this->db->delete('post');

		if ($res) {
			return true;
		}
		return false;
	}

	public function get_all_posts() {
		$id = $_SESSION['user']['userid'];
		$sql = "SELECT
					friendid as id
				FROM
					friend
				WHERE
					userid = ?";
		$query = $this->db->query($sql, [$id]);
		$friends = $query->result_array();

		$all_posts = [];

		foreach ($friends as $friend) {
			$sql = "SELECT
						*
					FROM
						post
							LEFT JOIN
						profile ON post.`from` = profile.userid
					WHERE
						`from` = ? AND `to` = ?";
			$all_posts[] = $this->db->query($sql, [$friend['id'], $friend['id']])->result_array();
		}

		return $all_posts;
	}

}
