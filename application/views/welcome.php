<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="welcome" class="container">
	<div class="login-form shadow-lg p-3 mb-5 bg-white rounded">
		<form action="/login" method="post">
			<img class="rotate" src="/img/logo_main.png">
			<h2 class="text-center">Psy Social</h2>
			<hr>
			<div class="form-group has-error">
				<input type="email" class="form-control" name="email" placeholder="Email" required="required">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="password" placeholder="Password" required="required">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
			</div>
			<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
		</form>
		<p class="text-center small text-center">Don't have an account? <a class="" href="/register">Sign up here!</a></p>
	</div>
</div>