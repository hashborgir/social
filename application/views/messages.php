<?php ?>

<div class="container">
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-received-tab" data-toggle="tab" href="#nav-received" role="tab" aria-controls="nav-received" aria-selected="true">Received</a>
			<a class="nav-item nav-link" id="nav-sent-tab" data-toggle="tab" href="#nav-sent" role="tab" aria-controls="nav-sent" aria-selected="false">Sent</a>
		</div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-received" role="tabpanel" aria-labelledby="nav-received-tab">
			<div class="container messages">
				<div class="row">
					<div class="col-md-4 friend-panel">
						<?php foreach ($inbox['received'] as $f) { ?>
							<div class="card card<?= html_escape($f['userid']) ?>" data-id="<?= html_escape($f['userid']) ?>">
								<div class="card-body">
									<div class="media align-items-center"><span style="background-image: url(<?= html_escape($f['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
										<div class="media-body overflow-hidden">
											<h5 class="card-text mb-0"><?= html_escape("{$f['firstname']} {$f['lastname']}  ") ?></h5>
											<p class="card-text text-uppercase text-muted">@<?= html_escape($f['username']) ?></p>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="col-md-8 messages-panel" style="">
						<div><h1>Reply <span id="message-to"></span></h1></div>
						<div class="">
							<div class="message-history"></div>
						</div>
						<div class="form-group">
							<textarea class="form-control" id="message" placeholder="Send Message" name="message"></textarea>
						</div>
						<button class="btn btn-primary btn-send">Send</button>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="nav-sent" role="tabpanel" aria-labelledby="nav-sent-tab">
			<div class="container messages">
				<div class="row">
					<div class="col-md-4 friend-panel">
						<?php foreach ($inbox['sent'] as $f) { ?>
							<div class="card card<?= html_escape($f['userid']) ?>" data-id="<?= html_escape($f['userid']) ?>">
								<div class="card-body">
									<div class="media align-items-center"><span style="background-image: url(<?= html_escape($f['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
										<div class="media-body overflow-hidden">
											<h5 class="card-text mb-0"><?= html_escape("{$f['firstname']} {$f['lastname']}  ") ?></h5>
											<p class="card-text text-uppercase text-muted">@<?= html_escape($f['username']) ?></p>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="col-md-8 messages-panel" style="">
						<div><h1>Reply <span id="message-to"></span></h1></div>
						<div class="">
							<div class="message-history"></div>
						</div>
						<div class="form-group">
							<textarea class="form-control" id="message" placeholder="Send Message" name="message"></textarea>
						</div>
						<button class="btn btn-primary btn-send">Send</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>