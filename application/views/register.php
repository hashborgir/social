<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="welcome" class="container">
	<div class="login-form shadow-lg p-3 mb-5 bg-white rounded">
		<form action="/register" method="post">
			<img class="rotate" src="/img/logo_main.png">
			<h2 class="text-center">Sign up</h2>
			<div class="form-group has-error">
				<input type="text" class="form-control" name="firstname" placeholder="First name" required="required">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" name="lastname" placeholder="Last name" required="required">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" name="email" placeholder="Your@email.com" required="required">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="password" placeholder="Password" required="required">

			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="password_repeat" placeholder="Password repeat" required="required">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Sign up</button>
			</div>
			<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
		</form>
		<p class="text-center small text-center">Already have an account? <a class="" href="/">Login here</a></p>
	</div>
</div>