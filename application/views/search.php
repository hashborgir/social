<div class="container">
	<h1 style="padding-top: 10px;">Users Found:  &nbsp; <?= ($results ? count($results) : ' 0') ?></h1>
	<hr>
	<div class="row">
		<?php
		if ($results) {


			foreach ($results as $m) {
				?>
				<div class="col-md-6 col-xl-4">
					<div class="card">
						<div class="card-body">
							<div class="media align-items-center"><span style="background-image: url(<?= ($m['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
								<div class="media-body overflow-hidden">
									<h5 class="card-text mb-0"><?= "{$m['firstname']} {$m['lastname']}  " ?></h5>
									<p class="card-text text-uppercase text-muted"><?= $m['username'] ?></p>
									<p class="card-text">

										<?= $m['email'] ?><br><abbr title="Phone">P:  </abbr>+1 <?= @$m['phone'] ?>
									</p>
								</div>
							</div><a href="/@<?= $m['username'] ?>" class="tile-link"></a>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>