<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!--ALL content comes after the above 3 tags-->
		<meta property="og:title" content="Psychedelic Social Network" />
		<meta property="og:image" content="https://www.psychedelicsdaily.com/img/hero-main.jpg" />

		<!-- OG Tags-->
		<meta property="og:title" content="" />
		<meta property="og:image" content="http://http://820f291f.ngrok.io/img/default.webp" />

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="/css/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js" ></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
		<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" ></script>

		<link href="/css/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


		<!-- FONT AWESOME OFFLINE-->
		<link rel="stylesheet" type="text/css" href="/css/fontawesome-free-5.11.2-web/css/all.min.css">
		<script src="/css/fontawesome-free-5.11.2-web/js/all.min.js"" ></script>

		<!-- CUSTOM CSS/JS-->
		<link rel="stylesheet" type="text/css" href="/css/custom.css">
		<script>

            var csrf_token_hash = '<?= html_escape($this->security->get_csrf_hash()) ?>';

            function send_message(id, message, call_get_messages = true) {
                $.ajax({
                    url: "/message/send/",
                    type: "post",
                    data: {
                        'id': id,
                        'message': message,
                        '<?= html_escape($this->security->get_csrf_token_name()) ?>': csrf_token_hash
                    },
                    success: function (response) {
                        $.get("/user/get_csrf_token_hash", function (data) {
                            csrf_token_hash = data.hash;
                        });
                        $('#message').val('');
                        if (call_get_messages) {
                            get_messages(id);
                        }
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            }

            function update_post(id) {
                var content = $('#stream-post-' + id + ' textarea').val();
                if (content == '') {
                    delete_post(id);
                } else {
                    $.ajax({
                        url: "/post/update/",
                        type: "post",
                        data: {
                            'id': id,
                            'content': content,
                            '<?= html_escape($this->security->get_csrf_token_name()) ?>': csrf_token_hash
                        },
                        success: function (response) {
                            $.get("/user/get_csrf_token_hash", function (data) {
                                csrf_token_hash = data.hash;
                            });
                        },
                        error: function (xhr) {
                            //Do Something to handle error
                        }
                    });

                }
            }
		</script>
		<script src="/js/index.js" ></script>
        <title>Psy Social</title>
    </head>
    <body>
		<?php $this->load->view('navbar', $user ?? null); ?>
		<select></select>
		<div class="spacer clearfix">
			&nbsp;
		</div>
		<?php if (!empty($this->session->flashdata('flashmessage'))) { ?>
			<div class="text-center flashmessage" style="">
				<h1 id="" class="" style="">
					<?= html_escape($this->session->flashdata('flashmessage')); ?>
				</h1>
			</div>
		<?php } ?>
		<div class="main">