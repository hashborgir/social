<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
	<div class="profile-wrapper">
		<div class="profile-section-user">
			<!-- /.profile-info-brief -->
			<div class="hidden-sm-down">
				<hr class="m-0">
				<div class="profile-info-contact p-4">
					<h2 class="profile-username mb-3"><?= html_escape("@$username") ?></h2>
					<?php if (!empty($about)) { ?><p><?= html_escape($about) ?></p><?php } ?>
					<table class="table">
						<tbody>
							<tr>
								<td><strong>Name:</strong></td>
								<td>
									<p class="text-muted mb-0"><?= html_escape("$firstname $lastname") ?></p>
								</td>
							</tr>
							<?php if (!empty($location)) { ?>
								<tr>
									<td><strong>Location:</strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$location") ?></p>
									</td>
								</tr>
							<?php } ?>

							<?php if (!empty($dob)) { ?>
								<tr>
									<td><strong>Date of Birth:</strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$dob") ?></p>
									</td>
								</tr>
							<?php } ?>
							<?php if (!empty($instagram)) { ?>
								<tr>
									<td><strong>Instagram: </strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$instagram") ?></p>
									</td>
								</tr>
							<?php } ?>
							<?php if (!empty($skype)) { ?>
								<tr>
									<td><strong>Skype: </strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$skype") ?></p>
									</td>
								</tr>
							<?php } ?>
							<?php if (!empty($relationship)) { ?>
								<tr>
									<td><strong>Relationship: </strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$relationship") ?></p>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- /.profile-info-contact -->
				<hr class="m-0">
				<div class="profile-info-general p-4">
					<h2 class="mb-3"> <i class="fas fa-mail-bulk"></i> Contact</h2>
					<table class="table">
						<tbody>
							<tr>
								<td><strong>Email:</strong></td>
								<td>
									<p class="text-muted mb-0"><?= html_escape("$email") ?></p>
								</td>
							</tr>
							<?php if (!empty($profession)) { ?>
								<tr>
									<td><strong>Job: </strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$profession") ?></p>
									</td>
								</tr>
							<?php } ?>
							<?php if (!empty($website)) { ?>
								<tr>
									<td><strong>Website:</strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$website") ?></p>
									</td>
								</tr>
							<?php } ?>
							<?php if (!empty($phone)) { ?>
								<tr>
									<td><strong>Phone: </strong></td>
									<td>
										<p class="text-muted mb-0"><?= html_escape("$phone") ?></p>
									</td>
								</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
				<!-- /.profile-info-general -->
				<hr class="m-0">
			</div>
			<!-- /.hidden-sm-down -->
		</div>
		<!-- /.profile-section-user -->
		<div class="container profile-section-main">
			<div class="send-dm bg-light" style="display:none">
				<h1><i class="fas fa-envelope" aria-hidden="true"></i> Send DM</h1>
				<div class="dm-editor">
					<textarea name="message" data-id="<?= intval($id) ?>" id="message" class="post-field" placeholder="Send DM @<?= html_escape($username) ?>"></textarea>
					<input type="hidden" name="<?= html_escape($csrf['name']); ?>" value="<?= html_escape($csrf['hash']); ?>" />
					<input id="ref" name="ref" type="hidden" value="<?= base_url(uri_string()) ?>">
					<input id="userid" name="userid" type="hidden" value="<?= intval($id) ?>">
					<div class="d-flex">
						<button class="btn btn-primary px-4 py-1 btn-send"><i class="fas fa-envelope"></i> Send</button>
						<button class="btn-dm-cancel btn px-4 py-1" style="background: pink; margin-left: 10px;"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
					</div>
				</div>
			</div>
			<h1> <i class="fas fa-user-clock" aria-hidden="true"> </i> Timeline</h1>
			<div class="post-editor">
				<form method="post" action="/post/create/<?= intval($id) ?>">
					<textarea name="post" id="post-field" class="post-field" placeholder="Write Something Cool!"></textarea>
					<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= html_escape($csrf['hash']); ?>" />
					<input id="ref" name="ref" type="hidden" value="<?= base_url(uri_string()) ?>">
					<div class="d-flex">
						<button class="btn btn-primary px-4 py-1"><i class="fa fa-comment" aria-hidden="true"></i>
							Post</button>
					</div>
				</form>
			</div>
			<?php foreach ($posts as $post) { ?>
				<!-- STREAM OF POSTS-->
				<div class="stream-posts">
					<!-- START INDIVIDUAL POST-->
					<div class="stream-post mb-0" id="stream-post-<?= intval($post['id']) ?>" data-id="<?= intval($post['id']) ?>">
						<div class="sp-author">
							<h5><a href="/@<?= html_escape($post['username']) ?>" class="sp-author-avatar"><img src="<?= (html_escape($post['image'])) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>" alt=""></a></h5>
							<h6 class="sp-author-name"><a href="/@<?= html_escape($post['username']) ?>"><?= html_escape($post['firstname']) . ' ' . html_escape($post['lastname']) ?><p>@<?= html_escape($post['username']) ?></p></a></h6>
						</div>
						<div style="padding-bottom: 10px" class="sp-post-info">
							<a style="margin-left: 70px; padding: 10px;" href="/@<?= html_escape($post['username']) ?>" class="h5 sp-author-avatar">@<?= $post['username'] ?></a>
							<span class="h6"><?= html_escape($post['date']) ?></span>
						</div>
						<div class="sp-content">
							<?php if ($_SESSION['user']['userid'] == $post['userid']) { ?>
								<textarea onchange="update_post(<?= html_escape($post['id']) ?>);" readonly style="width: 100%; border: none;" name="post-content form-control" class="sp-paragraph"><?= urldecode($post['post']); ?></textarea>
								<button onclick="delete_post(<?= $post['id'] ?>);" class="float-right px-2 py-0 btn-delete-post btn btn-primary" ><i class="fa fa-trash" aria-hidden="true"></i>
								</button>
							<?php } else { ?>
								<p style="width: 100%; border: none;" name="post-content" class="sp-paragraph"><?= urldecode($post['post']); ?></p>
							<?php } ?>
						</div>
					</div>
					<!-- START INDIVIDUAL POST-->
				</div>
				<!-- END STREAM OF POSTS -->
			<?php } ?>
		</div>
		<!-- /.profile-section-main -->
	</div>
</div>