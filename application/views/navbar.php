
<?php if ($this->session->user['logged_in']) { ?>
	<nav class="navbar navbar-icon-top navbar-expand-lg" id="topnav">
		<div class="navbar-brand-container" style="margin-left: 10px;">
			<a class="navbar-brand rotate" href="<?= ($this->session->user['logged_in'] ? '/home' : '/'); ?>"><i class="fas fa-2x fa-om"></i></a>
		</div>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i>
			</span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="/home">
					<!--<img style="display:block;height: 28px;margin-bottom: 2px; width: auto;" src="/img/logo.png" class="psychedelic">-->
						<i class="fas fa-2x fa-home"></i>
						<div>
							Home
						</div>
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/profile">
						<i class="fas fa-2x fa-user"></i>
						<div>
							<?= html_escape($this->session->user['firstname']) ?>
						</div>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-inbox nav-link" href="/messages">
						<i class="fas fa-2x fa-envelope"></i>
						<div>
							Inbox
							<sup style="" class="badge badge-danger"><?= ((!empty($inbox['unread_count'])) && ($inbox['unread_count'] > 0) ? html_escape($inbox['unread_count']) : ''); ?></sup>
						</div>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/friends">
						<i class="fas fa-2x fa-users" aria-hidden="true"></i>
						<div>Friends</div>
					</a>
				</li>
			</ul>
			<ul class = "navbar-nav ">
				<li class="nav-item">
					<a class="nav-link btn-logout"href="/settings">
						<i class="fas fa-2x fa-cog"></i>
						<div>Settings</div>
					</a>
				</li>
				<?php if ($_SESSION['user']['admin']) { ?>
					<li class="nav-item">
						<a class="nav-link btn-logout" href="/admin">
							<i class="fas fa-2x fa-cogs"></i>
							<div>Admin</div>
						</a>
					</li>
				<?php } ?>
				<li class="nav-item">
					<a class="nav-link btn-logout" href="/logout">
						<i class="fas fa-2x fa-sign-out-alt"></i>
						<div>Logout</div>
					</a>
				</li>
			</ul>
			<form class = "form-inline my-2 my-lg-0" method="post" action="/profile/search">
				<input name="q" class = "form-control mr-sm-2" type = "text" placeholder = "Search" aria-label = "Search">
				<button class = "btn btn-primary my-2 my-sm-0" type = "submit">Search</button>
				<input type="hidden" name="<?= html_escape($csrf['name']); ?>" value="<?= html_escape($csrf['hash']); ?>" />
			</form>

		</div>
	</nav>
<?php } ?>
