<?php ?>


<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-lg-12">
            <div class="main-box no-header clearfix">
                <div class="main-box-body clearfix">
					<?= $pagination ?>
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                                <tr>
									<th><span>id</span></th>
									<th><span>User</span></th>
									<th><span>Joined</span></th>
									<th class="text-center"><span>Status</span></th>
									<th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php foreach ($users as $u) { ?>
									<tr>
										<td><?= html_escape($u['id']); ?></td>
										<td>
											<img clsas="avatar float-left" src="<?= (html_escape($u['image']) ?? 'https://bootdey.com/img/Content/user_3.jpg' ); ?>" alt="" style="height: 32px; width: 32px;">
											<a href="/@<?= html_escape($u['username']); ?>" class="user-link"><?= html_escape("{$u['firstname']} {$u['lastname']}") ?></a>
											<span class="user-subhead">@<?= html_escape($u['username']); ?></span>
											<span class="user-subhead text-danger"><?= html_escape($u['email']); ?></span>
										</td>
										<td><?= $u['joindate']; ?></td>
										<td class="text-center">
											<span class="label label-default">Active: <?= ($u['admin'] ? '<span style="color: red;">Admin</span>' : 'Member') ?></span>
										</td>

										<td style = "width: 20%;">
											<a href = "#" class = "table-link">
												<span class = "fa-stack">
													<i class = "fa fa-square fa-stack-2x"></i>
													<i class = "fa fa-search-plus fa-stack-1x fa-inverse"></i>
												</span>
											</a>
											<a href = "#" class = "table-link">
												<span class = "fa-stack">
													<i class = "fa fa-square fa-stack-2x"></i>
													<i class = "fa fa-pencil fa-stack-1x fa-inverse"></i>
												</span>
											</a>
											<a href = "#" class = "table-link danger">
												<span class = "fa-stack">
													<i class = "fa fa-square fa-stack-2x"></i>
													<i class = "fa fa-trash-o fa-stack-1x fa-inverse"></i>
												</span>
											</a>
										</td>
									</tr>
									<?php
								}
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>