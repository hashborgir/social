<?php ?>
<div class="container db-social">
	<div class="jumbotron jumbotron-fluid"></div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11">
				<div class="widget head-profile has-shadow">
					<div class="widget-body pb-0">
						<div class="row d-flex align-items-center">
							<div class="col-xl-4 col-md-4 d-flex justify-content-center">
								<div class="image-default">
									<a href="/@<?= html_escape($user['profile']['username']) ?>"><img class="rounded-circle" src="<?= (html_escape($user['profile']['image'])) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>" alt=""></a>
								</div>
								<div class="infos">
									<h2><?= html_escape($user['profile']['firstname']) . " " . html_escape($user['profile']['lastname']); ?>
									</h2>
									<h4>
										<?= html_escape("@{$user['profile']['username']}") ?>
									</h4>

									<?php if (!$user['profile']['is_friend']) { ?>
										<button class="btn btn-shadow btn-warning add-friend btn-follow" onclick="add_friend(<?= html_escape($user['profile']['id']); ?>)"><i class="fa fa-user-plus"></i> Follow</button>
									<?php } ?>
								</div>
							</div>
							<div class="col-xl-4 col-md-4 d-flex justify-content-lg-start justify-content-md-start justify-content-center">
								<ul style="display: inline-flex;">
									<?php if (!is_home() && !is_profile() && ($user['profile']['username'] !== $_SESSION['user']['username'])) { ?>
										<li>
											<a data-id="<?= html_escape($user['profile']['id']); ?>" class="btn-send-dm text-primary user-id-<?= html_escape($user['profile']['id']); ?>"><i class="fa fa-envelope fa-4x" style="color: darkseagreen" aria-hidden="true"></i>
											</a>
											<div class="dm">@</div>
											<div class="heading">Message</div>
										</li>
									<?php } ?>
									<li>
										<a href="/friends"><i class="fa fa-4x fa-users" aria-hidden="true"></i>

											<div class="counter counter-following"><?= html_escape($user['following_count']) ?></div>
											<div class="heading">Following</div>
										</a>
									</li>

									<li>
										<a href="/friends"><i style="color: skyblue" class="fa fa-4x fa-user-friends" aria-hidden="true"></i>
											<div class="counter counter-followers"><?= html_escape($user['followers_count']) ?></div>
											<div class="heading">Followers</div>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>