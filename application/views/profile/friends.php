<div class="container">
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-followers-tab" data-toggle="tab" href="#nav-followers" role="tab" aria-controls="nav-followers" aria-selected="true">Followers</a>
			<a class="nav-item nav-link" id="nav-following-tab" data-toggle="tab" href="#nav-following" role="tab" aria-controls="nav-following" aria-selected="false">Following</a>
		</div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-followers" role="tabpanel" aria-labelledby="nav-followers-tab">
			<h1 style="padding-top: 10px;">Followers</h1>
			<hr>
			<div class="row">
				<?php
				foreach ($user['followers'] as $m) {
					?>
					<div class="col-md-3 col-xl-4">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center"><span style="background-image: url(<?= ($m['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
									<div class="media-body overflow-hidden">
										<h5 class="card-text mb-0"><?= "{$m['firstname']} {$m['lastname']}  " ?></h5>
										<p class="card-text">@<?= $m['username'] ?></p>
										<p class="card-text">

											<?= $m['email'] ?><br><abbr title="Phone">P:  </abbr>+1 <?= @$m['phone'] ?>
										</p>
									</div>
								</div><a href="/@<?= $m['username'] ?>" class="tile-link"></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>

		</div>
		<div class="tab-pane fade" id="nav-following" role="tabpanel" aria-labelledby="nav-following-tab">
			<h1 style="padding-top: 10px;">Following</h1>
			<hr>
			<div class="row">
				<?php
				foreach ($user['following'] as $m) {
					?>
					<div class="col-md-3 col-xl-4">
						<div class="card">
							<div class="card-body">
								<div class="media align-items-center"><span style="background-image: url(<?= ($m['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
									<div class="media-body overflow-hidden">
										<h5 class="card-text mb-0"><?= "{$m['firstname']} {$m['lastname']}  " ?></h5>
										<p class="card-text">@<?= $m['username'] ?></p>
										<p class="card-text">

											<?= $m['email'] ?><br><abbr title="Phone">P:  </abbr>+1 <?= @$m['phone'] ?>
										</p>
									</div>
								</div><a href="/@<?= $m['username'] ?>" class="tile-link"></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>

		</div>
	</div>
</div>