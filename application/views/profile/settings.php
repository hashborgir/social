<div class="container">
<div class="bootstrap-iso">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-6">

				<form method="post" action="/profile/update" id="#profileForm" enctype="multipart/form-data">
					<div class="form-group ">
						<h1>Profile</h1>
						<input class="form-control" id="firstname" name="firstname" placeholder="First name" type="text"/>
						<input class="form-control" id="lastname" name="lastname" placeholder="Last name" type="text"/>
						<input class="form-control" id="username" name="username" placeholder="Username" type="text"/>
						<textarea class="form-control" id="about" name="about" rows="4" placeholder="About myself"></textarea>
						<input class="form-control" id="relationship" name="relationship" placeholder="Relationship Status" type="text"/>
						<input class="form-control" id="date" name="dob" placeholder="Date of Birth" type="text"/>
						<input class="form-control" id="profession" name="profession" placeholder="Profession" type="text"/>
					</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				<div class="form-group">
					<h1>Contact</h1>
					<input class="form-control" id="website" name="website" placeholder="Website" type="text"/>
					<input class="form-control" id="skype" name="skype" placeholder="Skype" type="text"/>
					<input class="form-control" id="instagram" name="instagram" placeholder="Instagram" type="text"/>



					<input class="form-control" id="location" name="location" placeholder="Location" type="text"/>

				</div>

				<input class="form-control" id="phone" name="phone" placeholder="Phone #" type="text"/>
				<div class="form-group">
					<h1>Images</h1>
					<div class="custom-file">
						<input name="image" type="file" class="form-control custom-file-input" id="customFile">
						<label class="custom-file-label" for="customFile">Upload profile photo!</label>
					</div>
				</div>


				<div class="form-group text-center">
					<div style="margin: 20px;">
						<button class="btn btn-primary" type="submit">
							Submit
						</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

</div>