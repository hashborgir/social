<?php ?>
<div class="container">

	<div class="container homesections">
		<div class="row">
			<div class="col">
				<h1>News Feed</h1>

				<?php
				foreach ($all_posts as $posts) {
					foreach ($posts as $post) {
						?>
						<!-- STREAM OF POSTS-->
						<div class="stream-posts">
							<!-- START INDIVIDUAL POST-->
							<div class="stream-post mb-0" id="stream-post-<?= $post['id'] ?>">
								<div class="sp-author">
									<h5><a href="/@<?= $post['username'] ?>" class="sp-author-avatar"><img src="<?= ($post['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>" alt=""></a></h5>
									<h6 class="sp-author-name"><a href="/@<?= $post['username'] ?>"><?= $post['firstname'] . ' ' . $post['lastname'] ?><p>@<?= $post['username'] ?></p></a></h6>
								</div>
								<div style="padding-bottom: 10px ">
									<a style="margin-left: 70px; padding: 10px;" href="/@<?= $post['username'] ?>" class="h5 sp-author-avatar">@<?= $post['username'] ?></a>
									<span class="h6"><?= $post['date'] ?></span>
								</div>
								<?php if ($_SESSION['user']['userid'] == $post['userid']) { ?>
									<div class="sp-content">
										<textarea onchange="update_post(<?= $post['id'] ?>);" readonly style="width: 100%; border: none;" name="post-content form-control" class="sp-paragraph"><?= urldecode($post['post']); ?></textarea>
									</div>
									<div class="btn-postcontrols text-right">

										<a class="float-right btn btn-danger px-2 py-0" href="/post/delete/<?= $post['id'] ?>"><i style="color: maroon;" class="fa fa-trash"></i></a>

									</div>
								<?php } else { ?>
									<div class="sp-content">
										<p style="width: 100%; border: none;" name="post-content" class="sp-paragraph"><?= urldecode($post['post']); ?></p>
									</div>

								<?php } ?>
							</div>
							<!-- START INDIVIDUAL POST-->
						</div>
						<!-- END STREAM OF POSTS -->
						<?php
					}
				}
				?>
			</div>
		</div>

	</div>
	<div class="container homesections">

		<h1>New Members</h1>
		<div class="row">
			<?php foreach ($members['newmembers'] as $m) { ?>
				<div class="col-md-6 col-xl-4">
					<div class="card">
						<div class="card-body">
							<div class="media align-items-center"><span style="background-image: url(<?= ($m['image']) ?? 'https://bootdey.com/img/Content/avatar/avatar6.png'; ?>)" class="avatar avatar-xl mr-3"></span>
								<div class="media-body overflow-hidden">
									<h5 class="card-text mb-0"><?= "{$m['firstname']} {$m['lastname']}  " ?></h5>
									<p class="card-text text-muted">@<?= $m['username'] ?></p>
									<p class="card-text">
										Joined: <?= substr($m['joindate'], 0, 10) ?>
									</p>
								</div>
								<?php if (!$m['is_friend']) { ?>
									<button style="z-index: 1000" class="float-right btn btn-shadow btn-warning add-friend-<?= $m['id'] ?> follow" onclick="add_friend(<?= $m['userid']; ?>)"><i class="fa fa-user-plus"> </i> Follow</button>
								<?php } ?>
							</div><a href="/@<?= $m['username'] ?>" class="tile-link"></a>

						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>