<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Profile_model', 'profile');
		$this->load->model('Friend_model', 'friend');
		$this->load->model('Post_model', 'post');
		$this->load->model('Message_model', 'message');

		$_SESSION['user']['unread'] = $this->profile->get_unread_message_count($_SESSION['user']['userid']);
	}

	public function index() {
		$this->view($_SESSION['user']['username']);
	}

	public function home() {

		$data['members']['newmembers'] = $this->listnewmembers();
		$data['members']['all_posts'] = $this->post->get_all_posts();

		$data['user']['followers_count'] = count($this->profile->get_followers($_SESSION['user']['userid']));
		$data['user']['following_count'] = count($this->profile->get_following($_SESSION['user']['userid']));

		$data['user']['profile'] = $this->profile->get_profile($_SESSION['user']['username']);
		$data['user']['profile']['is_friend'] = true;

		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
		$data['user']['csrf'] = $this->csrf;

		$this->load->view('header', $data);
		$this->load->view('hero', $data);
		$this->load->view('profile/home', $data['members']);
		$this->load->view('footer');
	}

	public function view($username) {
		if (!empty($username)) {
			$data['user']['profile'] = $this->profile->get_profile($username);
			$data['user']['profile']['posts'] = $this->post->read($data['user']['profile']['id']);

			$data['user']['followers_count'] = count($this->profile->get_followers($data['user']['profile']['id']));
			$data['user']['following_count'] = count($this->profile->get_following($data['user']['profile']['id']));

			// check if currently logged in user's profile
			if (($data['user']['profile']['userid'] == $_SESSION['user']['userid'])) {
				$data['user']['profile']['is_friend'] = true;
			} else {
				$data['user']['profile']['is_friend'] = $this->is_friend($data['user']['profile']['id']);
			}

			$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
			$data['user']['csrf'] = $this->csrf;

			$this->load->view('header', $data);
			$this->load->view('hero', $data);
			$this->load->view('profile', $data['user']['profile']);
			$this->load->view('footer');
		}
	}

	public function is_friend($id) {
		return $this->friend->is_friend($id, $_SESSION['user']['userid']);
	}

	public function addfriend($id) {

		if ($id !== $_SESSION['user']['userid']) {
			if ($res = $this->friend->addfriend($id)) {
				header('Content-Type: application/json');
				echo json_encode($res);
			}
		}
		return false;
	}

	public function listnewmembers() {
		$newmembers = $this->friend->listnewmembers();

		foreach ($newmembers as $k => $v) {
			$id = $v['userid'];
			$newmembers[$k]['is_friend'] = $this->is_friend($id);
		}

		return $newmembers;
	}

	public function update() {
		if ($this->profile->username_exists($this->input->post('username'))) {
			$this->session->set_flashdata('flashmessage', 'Username already taken!');
			header('Location: /settings');
		} else
		if ($this->profile->update($_SESSION['user']['id'], $this->input->post())) {
			header('Location: /settings');
			$this->session->set_flashdata('flashmessage', 'Updated');
		} else {
			$this->session->set_flashdata('flashmessage', 'Failed to update');
			header('Location: /settings');
		}
	}

	public function friends() {
		$data['user']['profile'] = $this->profile->get_profile($_SESSION['user']['username']);
		$data['user']['profile']['is_friend'] = true;

		$data['user']['followers_count'] = count($this->profile->get_followers($_SESSION['user']['userid']));
		$data['user']['following_count'] = count($this->profile->get_following($_SESSION['user']['userid']));

		$data['user']['followers'] = $this->profile->get_followers($_SESSION['user']['userid']);
		$data['user']['following'] = $this->profile->get_following($_SESSION['user']['userid']);

		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
		$data['user']['csrf'] = $this->csrf;

		$this->load->view('header', $data);
		$this->load->view('hero', $data);
		$this->load->view('profile/friends', $data);
		$this->load->view('footer');
	}

	public function settings() {
		$data['user']['profile'] = $this->profile->get_profile($_SESSION['user']['username']);

		$data['user']['profile']['is_friend'] = true;

		$data['user']['followers_count'] = count($this->profile->get_followers($_SESSION['user']['userid']));
		$data['user']['following_count'] = count($this->profile->get_following($_SESSION['user']['userid']));

		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
		$data['user']['csrf'] = $this->csrf;

		$this->load->view('header', $data);
		$this->load->view('hero', $data);
		$this->load->view('profile/settings');
		$this->load->view('footer');
	}

	public function search() {
		$data['results'] = $this->profile->search(trim($this->input->post('q')));
		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
		$data['user']['csrf'] = $this->csrf;
		$this->load->view('header', $data);
		$this->load->view('search', $data);
		$this->load->view('footer');
	}

}
