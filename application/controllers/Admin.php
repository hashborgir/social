<?php

class Admin extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('rb');
		$this->load->model('Admin_model', 'admin');
		$this->load->model('Profile_model', 'profile');
		$this->load->model('Message_model', 'message');

		if (!$_SESSION['user']['admin']) {
			header('Location: /');
		}
	}

	public function index() {
		$config['base_url'] = $config['base_url'] = '/admin/index/';
		$config['uri_segment'] = $this->uri->segment(3) ?? 1;
		$config['num_links'] = 100;
		$config['total_rows'] = $this->admin->count_all_users();
		$config['per_page'] = 100;
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);

		$limit = $config['per_page'];

		$offset = (($limit * $config['uri_segment']) - $limit);

		$data['pagination'] = $this->pagination->create_links();
		$data['users'] = $this->admin->get_all_users($offset, $limit);

		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);
		$data['user']['csrf'] = $this->csrf;

		$this->load->view('header', $data);

		$this->load->view('admin', $data);
		$this->load->view('footer');
	}

	public function add_dummy_data($num = 1) {
		$faker = Faker\Factory::create();

		$r = ['Single', 'Married', 'In a relationship', 'Divorced', 'Seperated', 'Domestic Partnership'];

		$dummy_profiles = [];
		for ($i = 1; $i <= $num; $i++) {
			$password = "pass";
			$hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);

			// user table
			$user = R::dispense('user');
			$user->email = $faker->email;
			$user->password = $hash;
			$id = R::store($user);

			// profile table
			$profile = R::dispense('profile');
			$profile->userid = $id;

			$profile->firstname = $faker->firstName;
			$profile->lastname = $faker->lastName;
			$profile->username = $profile->firstname . $profile->lastname;
			$profile->joindate = date('Y-m-d', strtotime('-' . mt_rand(0, 1800) . ' days'));

			$profile->location = "$faker->city, $faker->country";
			$profile->image = "https://www.bootdey.com/img/Content/avatar/avatar" . rand(1, 6) . ".png";
			$profile->about = $faker->realText($maxNbChars = 200, $indexSize = 2);
			$profile->website = $faker->domainName;
			$profile->skype = $faker->domainWord;
			$profile->profession = $faker->jobTitle;

			$profile->relationship = $r[array_rand($r)];
			$profile->dob = $faker->dateTimeThisCentury->format('Y-m-d');
			$profile->phone = $faker->phoneNumber;

			$pid = R::store($profile);

			$dummy_profiles[] = $this->profile->get_profile_by_id($id);
		}
		var_dump($dummy_profiles);
	}

}
