<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {

		if (@$_SESSION['user']['logged_in']) {
			header('Location: /home');
		} else {
			$this->csrf['csrf'] = array(
				'name' => $this->security->get_csrf_token_name(),
				'hash' => $this->security->get_csrf_hash()
			);
			$this->load->view('header');
			$this->load->view('welcome', $this->csrf);
			$this->load->view('footer');
		}
	}

}
