<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// $this->output->enable_profiler(TRUE);

		$this->load->model('User_model', 'user');
		$this->load->model('Profile_model', 'profile');

		$this->email = $this->input->post('email');
		$this->password = $this->input->post('password');
		$this->password_repeat = $this->input->post('password_repeat');
		$this->firstname = $this->input->post('firstname');
		$this->lastname = $this->input->post('lastname');

		$this->csrf['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('register');
		$this->load->view('footer');
	}

	public function login() {
		if (!empty($this->email)) {
			$id = $this->user->verify_login($this->email, $this->password);
			if (!empty($id)) {
				$_SESSION['user'] = $this->profile->get_profile_by_id($id);

				$_SESSION['user']['logged_in'] = true;
				header('Location: /home');
			} else {
				$this->session->set_flashdata('flashmessage', "Account does not exist.");
				header('Location: /');
			}
		}
	}

	public function register() {
		if ($this->password !== $this->password_repeat) {
			$this->session->set_flashdata('flashmessage', "Passwords must match!");
			header('Location: /register');
		}

		if (!empty($this->email) && !empty($this->password) && !empty($this->password_repeat) && !empty($this->firstname) && !empty($this->lastname) && $this->password == $this->password_repeat) {

			$res = $this->user->add($this->email, $this->password, $this->firstname, $this->lastname);
			var_dump($res);
			if ($res) {
				$this->session->set_flashdata('flashmessage', "Account created.");
				header('Location: /');
			} else {
				$this->session->set_flashdata('flashmessage', "Account exists.");
			}
		}
		$this->load->view('header');
		$this->load->view('register', $this->csrf);
		$this->load->view('footer');
	}

	public function logout() {
		$this->session->sess_destroy();
		header('Location: /');
	}

	public function reset() {
		echo "password reset: todo";
	}

	public function get_csrf_token_hash() {
		header('Content-Type: application/json');
		echo json_encode(array("hash" => $this->security->get_csrf_hash()));
	}

}
