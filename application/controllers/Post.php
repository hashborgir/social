<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Post_model', 'post');
	}

	public function create($id) {
		$post = $this->input->post('post');

		if (empty($post)) {
			header('Location: ' . $this->input->post('ref'));
		} else {
			$this->post->create($id, $post);
			header('Location: ' . $this->input->post('ref'));
		}
	}

	public function read($id) {
		return $this->post->read($id);
	}

	public function update() {
		$id = $this->input->post('id');
		$content = $this->input->post('content');
		if ($id) {
			echo $this->post->update($id, $content);
		}
	}

	public function delete($id) {
		$this->post->delete($id);
		$this->session->set_flashdata('flashmessage', "Post deleted!");
		header('Location: /profile');
	}

}
