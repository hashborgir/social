<?php

class Message extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Profile_model', 'profile');
		$this->load->model('Friend_model', 'friend');
		$this->load->model('Message_model', 'message');
	}

	public function index() {
		$data['user']['profile'] = $this->profile->get_profile($_SESSION['user']['username']);
		$data['user']['profile']['is_friend'] = true;

		$data['user']['followers_count'] = count($this->profile->get_followers($_SESSION['user']['userid']));
		$data['user']['following_count'] = count($this->profile->get_following($_SESSION['user']['userid']));

		$data['user']['inbox'] = $this->message->get_inbox();
		$data['user']['inbox']['unread_count'] = $this->message->get_unread($_SESSION['user']['userid']);

		$data['user']['csrf'] = $this->csrf;

		$this->load->view('header', $data);
		$this->load->view('hero', $data);

		$this->load->view('messages', $data);
		$this->load->view('footer');
	}

	public function send() {
		$id = $this->input->post('id');
		$message = $this->input->post('message');

		$this->message->send($id, $message);
		return true;
	}

	public function mark_as_read($id) {
		$_SESSION['inbox']['unread'] = $_SESSION['inbox']['unread'] - 1;
		return $this->message->mark_as_read($id);
	}

	public function get_messages($id) {
		header('Content-Type: application/json');
		echo json_encode($this->message->get_messages($id));
	}

}
