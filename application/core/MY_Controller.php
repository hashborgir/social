<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		// $this->output->enable_profiler(TRUE);

		if (!$_SESSION['user']['logged_in']) {
			header('Location: /');
		}
	}

}
