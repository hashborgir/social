<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

function dd($var) {
	var_dump($var);
	die();
}

function d($var) {
	var_dump($var);
}

function is_home() {
	$CI = & get_instance();
	return ($CI->uri->segment(1) == 'home') ? TRUE : FALSE;
}

function is_profile() {
	$CI = & get_instance();
	return ($CI->uri->segment(1) == 'profile') ? TRUE : FALSE;
}