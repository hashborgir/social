function get_messages(id) {
    // get messages
    $.get("/message/get_messages/" + id, function (data) {
        $('.message-history').empty();
        data.forEach(function (i, v) {
            if (i.viewed == 0) {
                var style = "background: #F9EEFF;";
                var unread = "unread";
            }
            var image = i.image || "https://bootdey.com/img/Content/avatar/avatar6.png";
            var string = `<div class = "stream-posts" >
							<!-- START INDIVIDUAL POST-->
							<div onclick="mark_as_read(${i.id})" data-id="${i.id}" class="${unread}-${i.id} stream-post mb-0 stream-post-${i.id}" style="${style} min-height: unset;">
								<div class="sp-author">
									<a href="/@${i.username}" class="sp-author-avatar"><img src="${image}" alt=""></a>
								</div>
								<div style="padding-bottom: 10px ">
									<a style="margin-left: 70px; padding: 10px;" href="/@>" class="h5 sp-author-avatar">@${i.username}</a>
									<span class="h6">${i.date}</span>
								</div>
								<div class="sp-content">
									<p class="sp-paragraph">${i.message}</p>
								</div>
							</div>
						</div>`;
            $('.message-history').append(string);
        });
    });
}

function mark_as_read(id) {
    $.get("/message/mark_as_read/" + id, function (data) {
        var unread = parseInt($('.nav-inbox .badge').text());
        if (unread > 0) {
            var new_unread = unread - 1;
            $('.nav-inbox .badge').text(new_unread);
            if (new_unread == 0) {
                $('.nav-inbox .badge').text('');
            }
        }
    });
    $('.unread-' + id).css('background', '#FFF');
}

if (window.location.href.indexOf("profile") > -1) {
    var profile = true;
}

function delete_post(id) {
    $.get(encodeURI("/post/delete/" + id), function (data) {
        $('#stream-post-' + id).remove();
    });
}

function add_friend(id) {
    $(".btn-follow, .add-friend-" + id).remove();
    $.get(encodeURI("/profile/addfriend/" + id), function (data) {
        if (window.location.href.includes('@')) {
            $(".counter-followers").text(data.current_user.followers);
            $(".counter-following").text(data.current_user.following);
        } else {
            $(".counter-followers").text(data.user.followers);
            $(".counter-following").text(data.user.following);
        }
    });
}

$(document).ready(function () {
    setTimeout(function () {
        $('.flashmessage').hide(500);
    }, 2000);

    $('.sp-content textarea').dblclick(function () {
        $(this).removeAttr('readonly')
    });
    $('.sp-content textarea').blur(function () {
        $(this).attr('readonly', 'readonly')
    });

    $('.user-list').DataTable();

    $('.messages .card').each(function () {
        var id = $(this).data('id');
        $(this).click(function () {
            $('#message-to').text($('.card' + id + ' p.card-text').text());
            $('#message').attr('data-id', id);
            get_messages(id);
            $('.messages-pane').hide().show();
        });
    })

    // send message
    $('.btn-send').click(function () {
        var id = encodeURIComponent($('#message').data('id'));
        var message = encodeURIComponent(($('#message').val()));
        if (message !== '' && window.location.href.includes('@')) {
            send_message(id, message, false);
        } else if (message !== '' && window.location.href.includes('messages')) {
            send_message(id, message, true);
        }
    });

    $('.btn-send-dm, .btn-dm-cancel').click(function () {
        $('.send-dm').slideToggle();
    });
});